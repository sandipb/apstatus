import os
from os.path import abspath, dirname, join as osjoin
import sys
from setuptools import setup

this_dir = abspath(dirname(__file__))

def read(fname):
    return open(osjoin(this_dir, fname)).read()

sys.path.insert(0,osjoin(this_dir, "src"))
import apstats
app_version = apstats.version

setup(
    name = "apstats",
    version = app_version,
    author = "Sandip Bhattacharya",
    author_email = "sandipb@sandipb.net",
    description = ("An Apache log file reading tool"),
    license = "Proprietary",
    keywords = "logs statistics apache",
    url = "https://github.com/sandipb/apstats",
    packages=['apstats', 'apstats.stats'],
    package_dir = {"":"src"},
    long_description=read('doc/README.rst'),
    scripts = ["apstats","src/utils/dump_ap_log.py"],
    test_suite='tests',
    tests_require=['nose'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "Environment :: Console",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2.7",
        "Topic :: Internet :: WWW/HTTP :: HTTP Servers",
        "Topic :: System :: Monitoring",
        "License :: Other/Proprietary License"
    ],
)
