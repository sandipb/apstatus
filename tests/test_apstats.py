
import unittest
import imp
from os.path import abspath, dirname
import libtest
from datetime import datetime
import apstats.stats as stats

exec_path = abspath(dirname(__file__)+"/../apstats")
imp.load_source("apstats_exec", exec_path)


import apstats_exec as ap #@UnresolvedImport

class TestApstats(unittest.TestCase):
    "Test the functionality of the main apstats script"

    def test_text_to_dt(self):
        "Test timestamp to datetime conversion"
        self.assertEqual(ap.text_to_dt("201211210430"),
                         datetime(2012,11,21,4,30)) 
        self.assertEqual(ap.text_to_dt("201211210440"),
                         datetime(2012,11,21,4,40)) 
        
        # no year
        text = "11210440"
        dt_today = datetime.today().replace(second=0, microsecond=0)
        dt = dt_today.replace(month=11, day=21, hour=4, minute=40, )
        self.assertEqual(ap.text_to_dt(text),dt, dt_today)
        
        # no month
        text = "210440"
        dt = dt_today.replace(day=21, hour=4, minute=40)
        self.assertEqual(ap.text_to_dt(text), dt)
        
        # no day
        text = "0440"
        dt = dt_today.replace(hour=4, minute=40)
        self.assertEqual(ap.text_to_dt(text),dt)
        
        # no hour
        self.assertRaises(ValueError, ap.text_to_dt, "40")
        self.assertRaises(ValueError, ap.text_to_dt, "4")
        
        # No minute!
        self.assertRaises(ValueError, ap.text_to_dt, "")
        
        # Not numeric
        self.assertRaises(ValueError, ap.text_to_dt, "11210440m")
                                                            

    def test_get_parameters(self):
        "Test the command line parameters processing of the script"
        if not stats.plugins_loaded:
            stats.load_plugins()
        log_path = libtest.get_test_data_path("l1_1000.log")
        
        # stats without parameters
        cmdline = ("-d qps,errors " +  log_path).split()
        opts = ap.get_parameters(cmdline)
        self.assertEqual(opts,
                         {
                          "filename": log_path,
                          "statsall": False,
                          "stats": [["qps"], ["errors"]]
                          })
        
        # stats with parameters
        cmdline = ("-d timed_qps:100,errors " +  log_path).split()
        opts = ap.get_parameters(cmdline)
        self.assertEqual(opts,
                         {
                          "filename": log_path,
                          "statsall": False,
                          "stats": [["timed_qps","100"], ["errors"]]
                          })
        
        # test limit
        time_samps = {
                      "201211210430-201211210440": [
                                                    datetime(2012,11,21,4,30),
                                                    datetime(2012,11,21,4,40),
                                                    ]
                      }
        
        for samp in time_samps:
            cmdline = ("-d qps -l %s %s" % (samp,log_path)).split()
            opts = ap.get_parameters(cmdline)
            self.assertEqual(opts,
                             {
                              "filename": log_path,
                              "stats": [["qps"]],
                              "statsall": False,
                              "limit": time_samps[samp]
                              })
    def test_parse_duration(self):
        "Test if parsing of continuous interval duration works"
        
        self.assertEqual(ap.parse_duration("30") , {"time": 30})
        self.assertEqual(ap.parse_duration("30s"), {"time": 30})
        self.assertEqual(ap.parse_duration("30m"), {"time": 30*60})
        self.assertEqual(ap.parse_duration("30h"), {"time": 30*60*60})
        self.assertEqual(ap.parse_duration("30l"), {"lines": 30})
        self.assertEqual(ap.parse_duration("30L"), {"lines": 30})
        self.assertRaises(ValueError, ap.parse_duration, "")
        self.assertRaises(ValueError, ap.parse_duration, "0")
        self.assertRaises(ValueError, ap.parse_duration, "30d")
        self.assertRaises(ValueError, ap.parse_duration, "30n")


if __name__ == "__main__":
    unittest.main()
    