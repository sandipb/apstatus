
import unittest
import libtest
import  apstats.stats
from apstats.parser import LogEntry

class TestStatsErrorList(unittest.TestCase):
    "Test the functionality of ApStats.stats.error_list.StatsErrorList"

    def setUp(self):
        if not apstats.stats.plugins_loaded:
            apstats.stats.load_plugins()

    def test_stats_error_list(self):
        log_path = libtest.get_test_data_path("snet_20120312.log")
        self.assertTrue("errors" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["errors"]()
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        # $ src/utils/parse_log.py 6 tests/data/snet_20120312.log | sort -n | uniq -c
        # 6332 200
        #    4 301
        #   13 302
        #  181 304
        #    1 404
        #    5 500

        self.assertEqual(stats.reqs, {"404": 1, "500": 5})


if __name__ == "__main__":
    unittest.main()
