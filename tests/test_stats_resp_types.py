
import unittest
import libtest
import apstats.stats
from apstats.parser import LogEntry

class TestStatsRespTypes(unittest.TestCase):
    "Test the functionality of apstats.stats.req_types.StatsRespType"

    def setUp(self):
        if not apstats.stats.plugins_loaded:
            apstats.stats.load_plugins()
        
    def test_stats_resp_types(self):
        log_path = libtest.get_test_data_path("l1_1000.log")
        self.assertTrue("res_types" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["res_types"]()
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        calc = stats.get_calculation()

        # $ src/utils/parse_log.py 6 tests/data/l1_1000.log| sort | uniq -c
        # 820 200
        # 180 404

        expected = [('200',820,round(820*100/1000,2)),
                    ('404',180,round(180*100/1000,2))]
        self.assertEqual(calc["response_list"], expected)
        self.assertEqual(stats.resp_count, 1000)


if __name__ == "__main__":
    unittest.main()
