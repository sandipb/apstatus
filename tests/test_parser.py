
import unittest
import libtest
from apstats.parser import LogEntry, ParserException
from datetime import datetime
#import logging

class TestLogEntry(unittest.TestCase):
    "Test the functionality of ApStats.Parser.LogEntry"

    def setUp(self):

        # Just interested in testing internal functions
        class DummyLogEntry(LogEntry):
            def __init__(self):
                pass
   
        self.dummyentry = DummyLogEntry() 
        
    def test_init(self):
        "Parsing of log file should be done properly"

        attrs = "client_ip client_ident client_auth_name \
                 time method uri path qs protocol status size referer ua orig_text".split()
        
        log_data = libtest.get_test_data("logfile1.txt").splitlines()

        # 66.249.67.197 - - [11/Mar/2012:00:26:02 -0800] 
        #           "GET /2009/02/12/getting-more-printable-pdfs-from-texinfo-manuals/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+SandipBhattacharya+(sandipb.net) HTTP/1.1" 
        #           200 6368 "-" 
        #           "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
        entry1 = LogEntry(log_data[0])
        
        for a in attrs:         
            self.assertTrue(hasattr(entry1, a), "LogEntry should have attribute '%s'" % a)

        self.assertEqual(entry1.client_ip,"66.249.67.197")
        self.assertEqual(entry1.client_ident,"-")
        self.assertEqual(entry1.client_auth_name,"-")
        self.assertEqual(entry1.time,datetime(2012,3,11,0,26,02))
        self.assertEqual(entry1.method,"GET")
        self.assertEqual(entry1.uri,
                         "/2009/02/12/getting-more-printable-pdfs-from-texinfo-manuals/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+SandipBhattacharya+(sandipb.net)")
        self.assertEqual(entry1.protocol,
                         "HTTP/1.1")

        # These tests are not a priority now
        self.assertEqual(entry1.path,
                         "/2009/02/12/getting-more-printable-pdfs-from-texinfo-manuals/")
        self.assertEqual(entry1.qs,
                         "utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+SandipBhattacharya+(sandipb.net)")        

        self.assertEqual(entry1.status, 200)
        self.assertEqual(entry1.size, 6368)
        self.assertEqual(entry1.referer, "-")
        self.assertEqual(entry1.ua,
                         "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)")
#        self.assertEqual(entry1.orig_text,
#                          log_data[0])
        
        
        # 85.25.100.162 - - [16/Mar/2012:11:21:52 -0700] "GET /wp-trackback.php HTTP/1.1" 
        #               404 726 "-" 
        #               "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"    
        input_txt = '172.16.1.190 - - [10/Mar/2012:04:28:08 -0800] \
                 "GET /edu/resources/school_directory_version.txt HTTP/1.1" \
                 200 19 "-" "Java/1.6.0_16"'
        entry1 = LogEntry(input_txt)
        self.assertEqual(entry1.client_ip,"172.16.1.190")
        self.assertEqual(entry1.client_ident,"-")
        self.assertEqual(entry1.client_auth_name,"-")
        self.assertEqual(entry1.time, datetime(2012,3,10,4,28,8))
        self.assertEqual(entry1.method,"GET")
        self.assertEqual(entry1.uri,
                         "/edu/resources/school_directory_version.txt")
        self.assertEqual(entry1.protocol,
                         "HTTP/1.1")
        self.assertEqual(entry1.status,
                         200)
        self.assertEqual(entry1.size,
                         19)
        self.assertEqual(entry1.referer,
                         "-")
        self.assertEqual(entry1.ua,
                         "Java/1.6.0_16")
       
    def test_parse_time(self):
        "Test LogEntry.parse_time()"
        
                    
        self.assertEqual(self.dummyentry.parse_time("[11/Mar/2012:00:26:02 -0800]"),
                         datetime(2012,3,11,0,26,02)) # Should not be a problem
        
        self.assertEqual(self.dummyentry.parse_time("[11/Mar/2012:00:26:02 -0700]"),
                         datetime(2012,3,11,0,26,02)) # Timezone differences shouldn't matter
        
        self.assertEqual(self.dummyentry.parse_time("[1/Mar/2012:00:26:02 -0700]"),
                         datetime(2012,3,1,0,26,02)) # For some reason even single digit dates work
        
#        logging.warning(self.dummyentry.parse_time("[1/Mar/2012:00:26:02 -0700]"))
        # # Bad input should fail. even subtle one here (date has a single digit)
        self.assertRaises(ParserException, self.dummyentry.parse_time,
                          "[1/03/2012:00:26:02 -0700]") 
    
    def test_parse_request(self):
        "Test LogEntry.parse_request()"

        test_sets = \
        {
          "GET /2012/01/19/pratap-bhanu-mehta-on-state-censorship-in-india/ HTTP/1.1":
            ["GET", 
             "/2012/01/19/pratap-bhanu-mehta-on-state-censorship-in-india/", "HTTP/1.1"],
         "GET /wp-trackback.php HTTP/1.1":
            ["GET","/wp-trackback.php", "HTTP/1.1"]
            
         }
        for inp, out in test_sets.items():
            self.assertEqual(self.dummyentry.parse_request(inp), out)
        
        