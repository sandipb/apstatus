
import unittest
import libtest
import  apstats.stats
from apstats.parser import LogEntry

class TestStatsURLsQPS(unittest.TestCase):
    "Test the functionality of apstats.stats.urls_qps.StatsURLsQPS"

    def setUp(self):
        if not apstats.stats.plugins_loaded:
            apstats.stats.load_plugins()

    def test_stats_error_list(self):
        log_path = libtest.get_test_data_path("l1_1000.log")
        self.assertTrue("urls_qps" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["urls_qps"]()
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        calc = stats.get_calculation()

 
        # # url count
        # $ src/utils/parse_log.py 5 tests/data/l1_1000.log| cut -d " " -f 2 |cut -f 1 -d '?'| sort| uniq -c| sort -nr | head
        #     267 /range/list
        #     262 /cgi-bin/range.py
        #     208 /edu/resources/school_directory_version.txt
        #      72 /geo/resources/version.properties
        #      36 /geo/resources/version_en_US.properties
        #      36 /geo/resources/version_en_US.class
        #      36 /geo/resources/version_en.properties
        #      36 /geo/resources/version_en.class
        #      36 /geo/resources/version.class
        #      11 /server-status
        #
        # # number of unique urls
        # $ src/utils/parse_log.py 5 tests/data/l1_1000.log| cut -d " " -f 2 |cut -f 1 -d '?'| sort| uniq -c| sort -nr| wc -l
        # 10
        #
        # # seconds between first and last line
        # $ (head -n 1 tests/data/l1_1000.log;tail -n 1  tests/data/l1_1000.log)| src/utils/parse_log.py 3 \
        #         | sed -e 's/\[//' -e 's/\//-/g' -e 's/:/ /' \
        #         | (read a ;read b; expr `date -d "$b" +%s` - `date -d "$a" +%s`)
        # 660
 
        test_data = """267 /range/list 262 /cgi-bin/range.py 208 /edu/resources/school_directory_version.txt 
                     72 /geo/resources/version.properties 36 /geo/resources/version_en_US.properties 
                     36 /geo/resources/version_en_US.class 36 /geo/resources/version_en.properties 
                     36 /geo/resources/version_en.class 36 /geo/resources/version.class 11 /server-status """.split()
        test_counts = [(test_data.pop(),int(test_data.pop())) for _ in range(len(test_data)/2)]
        self.maxDiff = None
        self.assertEqual(stats.url_reqs,dict(test_counts))
        self.assertEqual(calc["duration"],660)
        
        self.assertEqual(calc["url_list"][0],["/range/list",267,267*1.0/660])


if __name__ == "__main__":
    unittest.main()
