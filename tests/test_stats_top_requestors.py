
import unittest
import libtest
import apstats.stats
from apstats.parser import LogEntry
from datetime import datetime

class TestStatsTopRequestors(unittest.TestCase):
    "Test the functionality of ApStats.stats.top_requestors.StatsTopRequestors"

    def setUp(self):
        if not apstats.stats.plugins_loaded:
            apstats.stats.load_plugins()
        
    def test_stats_top_requestors(self):
        log_path = libtest.get_test_data_path("l1_1000.log")
        self.assertTrue("top_ips" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["top_ips"]()
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        calc = stats.get_calculation()

        # $ cut -f 1 -d " " l1_1000.log | sort  | uniq -c | sort -nr | head -n 10
        # 11 ::1
        #  9 172.16.1.72
        #  9 172.16.1.30
        #  9 172.16.1.212
        #  9 172.16.1.208
        #  9 172.16.1.195
        #  9 172.16.1.188
        #  9 172.16.1.177
        #  9 172.16.1.123
        #  9 172.16.1.117

        expected = """::1          172.16.1.72  172.16.1.30  172.16.1.212 172.16.1.208
                      172.16.1.195 172.16.1.188 172.16.1.177 172.16.1.123 172.16.1.117""".split()
        self.assertEqual(calc["top"], expected)
        
    def test_stats_top_requestors_limit(self):
        log_path = libtest.get_test_data_path("l1_1000.log")
        self.assertTrue("top_ips" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["top_ips"](5)
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        calc = stats.get_calculation()

        # $ cut -f 1 -d " " l1_1000.log | sort  | uniq -c | sort -nr | head -n 10
        # 11 ::1
        #  9 172.16.1.72
        #  9 172.16.1.30
        #  9 172.16.1.212
        #  9 172.16.1.208
        #  9 172.16.1.195
        #  9 172.16.1.188
        #  9 172.16.1.177
        #  9 172.16.1.123
        #  9 172.16.1.117

        expected = """::1          172.16.1.72  172.16.1.30  172.16.1.212 172.16.1.208
                      172.16.1.195 172.16.1.188 172.16.1.177 172.16.1.123 172.16.1.117""".split()
        self.assertEqual(calc["top"], expected[:5])


if __name__ == "__main__":
    unittest.main()
