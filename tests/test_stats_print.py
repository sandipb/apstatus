
import unittest
import libtest
import apstats.stats
from apstats.parser import LogEntry

class TestStatsPrint(unittest.TestCase):
    "Test the functionality of ApStats.stats.qps.StatsQPS"

    def setUp(self):
        if not apstats.stats.plugins_loaded:
            apstats.stats.load_plugins()
        
    def test_statsprint(self):
        log_data = libtest.get_test_data("l1_1000.log")
        log_path = libtest.get_test_data_path("l1_1000.log")
        self.assertTrue("print" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["print"]()
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        self.assertEqual(stats.num_lines, 1000)
        calc = stats.get_calculation()
        self.assertEqual(calc["lines"], log_data.splitlines())

if __name__ == "__main__":
    unittest.main()