
import unittest
import libtest
import apstats.stats
from apstats.parser import LogEntry
from datetime import datetime

class TestStatsTimedQPS(unittest.TestCase):
    "Test the functionality of ApStats.stats.qps.StatsTimesQPS"

    def setUp(self):
        if not apstats.stats.plugins_loaded:
            apstats.stats.load_plugins()
        
    def test_statsqps(self):
        log_path = libtest.get_test_data_path("l1_1000.log")
        self.assertTrue("timed_qps" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["timed_qps"]()
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        self.assertEqual(stats.num_queries, 1000)
        calc = stats.get_calculation()

        # # number of lines falling into each 5 minute interval
        # $ src/utils/parse_log.py 3 tests/data/l1_1000.log | cut -d : -f 2,3,4 | tr   : " "| perl -ne '@a=split;print $a[0],":",$a[1] - ($a[1] % 5),"\n"' | uniq -c
        # 159 04:25
        # 465 04:30
        # 376 04:35
        #
        # $ head -n 1 tests/data/l1_1000.log | src/utils/parse_log.py 3
        # [10/Mar/2012:04:28:02
  
        zones_expected = [ 
                            [datetime(2012,03,10,04,25), 159, 159.0/300],
                            [datetime(2012,03,10,04,30), 465, 465.0/300],
                            [datetime(2012,03,10,04,35), 376, 376.0/300]
                         ]
        total_time = (datetime(2012,03,10,04,35) - datetime(2012,03,10,04,25)).total_seconds()
        total_time += 300 # The time span goes till the end of the last zone
        expected_avg_qps = 1000.0/ total_time 
        expected_min_qps = 159.0/300
        expected_max_qps = 465.0/300

        self.assertEqual(calc["zone_list"], zones_expected)
        self.assertEqual(calc["avg_qps"], expected_avg_qps)
        self.assertEqual(calc["min_qps"], expected_min_qps)
        self.assertEqual(calc["max_qps"], expected_max_qps)


if __name__ == "__main__":
    unittest.main()
