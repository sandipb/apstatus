
import unittest
import libtest
from apstats.logreader import LogReader, LogReaderException, TailLogReader
from StringIO import StringIO
import sys
import os
import tempfile
#import logging

class TestLogReader(unittest.TestCase):
    "Test the functionality of apstats.logreader.LogReader"

    def test_init_noinput(self):
        "Test whether LogReader will complain if no input method is specified"
        self.assertRaises(LogReaderException, LogReader)

    def test_init_stdin(self):
        "Test whether LogReader will read from stdin if specified in __init__"
        
        rdr = LogReader(stdin=True)
        self.assertEqual(rdr.file_handle,sys.stdin)#       "Data sent to stdin will be read by LogReader if __init__(stdin=True")
       
    def test_init_filehandle(self):
        "Test whether LogReader will read from filehandle if specified in __init__"
        
        log_data = libtest.get_test_data("logfile1.txt")
        s = StringIO(log_data)
        rdr = LogReader(file_handle=s)
        out = rdr.file_handle.read()
        s.close()
        self.assertEqual(out, log_data) 
       
    def test_init_filename(self):
        "Test whether LogReader will read from filename if specified in __init__"
        
        log_data = libtest.get_test_data("logfile1.txt")
        log_path = libtest.get_test_data_path("logfile1.txt")
        rdr = LogReader(file_name=log_path)
        out = rdr.file_handle.read()
        self.assertEqual(out, log_data)
        rdr.file_handle.close() 
       
class TestTailLogReader(unittest.TestCase):
    "Test the functionality of apstats.logreader.TailLogReader"
       
    def setUp(self):
        self.log_data = libtest.get_test_data("logfile1.txt")
        
        fh,fn = tempfile.mkstemp(prefix="test_logreader.")
        os.write(fh,self.log_data)
        os.close(fh)
        self.templog = fn

    def tearDown(self):
        os.unlink(self.templog)

    def test_readline_init(self):
        "On init should go to end of file and return the same as EOF"                
        rdr = TailLogReader(file_name=self.templog)
        self.assertEqual(rdr.readline(), "")
        
    def test_readline_readone(self):   
        "First readline should return the first line written to file after init"             
        rdr = TailLogReader(file_name=self.templog)
        self.assertEqual(rdr.readline(), "")
        f = open(self.templog,"a")
        f.write(self.log_data)
        f.close()
        self.assertEqual(rdr.readline(), self.log_data.split("\n")[0].strip())

    def test_readline_readpartial(self):
        "Partial lines should not be returned and should appear after completion of the line"                
        rdr = TailLogReader(file_name=self.templog)
        self.assertEqual(rdr.readline(), "")
        
        data = self.log_data.splitlines(True)
        f = open(self.templog,"a")
        f.write(data[0])
        line = data[1]
        line_first = line[:5]
        line_last = line[5:]
        f.write(line_first)
        f.close()
        
        self.assertEqual(rdr.readline(), data[0].strip()) # first line should be complete
        self.assertEqual(rdr.readline(), "") # partial line shouldn't be returned
        f = open(self.templog,"a")
        f.write(line_last) # finish the partial line
        f.close()
        self.assertEqual(rdr.readline(), line.strip()) # now previously partial line should be returned
        self.assertEqual(rdr.readline(), "") # partial line shouldn't be returned
        
