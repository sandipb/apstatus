
import unittest
import tempfile
import shutil
import os
import sys
from apstats import stats
from datetime import datetime
#import logging
#import subprocess

class TestStats(unittest.TestCase):
    "Test the functionality of apstats.stats"

    def setUp(self):
        self.tempdir = tempfile.mkdtemp(prefix="apstats.teststats")
        os.makedirs(os.path.join(self.tempdir,"teststats", "plugins"))
        open(os.path.join(self.tempdir,"teststats","__init__.py"), "w").close()
        open(os.path.join(self.tempdir,"teststats","plugins","__init__.py"), "w").close()
        open(os.path.join(self.tempdir,"teststats","plugins","moda.py"), "w").close()
        open(os.path.join(self.tempdir,"teststats","plugins","modb.py"), "w").close()
        #logging.warning(subprocess.Popen("find " + self.tempdir, shell=True,stdout=subprocess.PIPE).communicate()[0])
        sys.path.insert(0,self.tempdir)
        self.stats_map_save = stats.stats_map
        self.plugins_loaded_save = stats.plugins_loaded
        stats.plugins_loaded = False
    
    def tearDown(self):
        shutil.rmtree(self.tempdir)
        del(sys.path[0])
        stats.stats_map = self.stats_map_save
        stats.plugins_loaded = self.plugins_loaded_save
        mod_names = sys.modules.keys() # don't iterate over a hash you will be modifying
        for modname in mod_names:
            if modname.startswith("teststats"):
                del(sys.modules[modname])
        
        
    def test_load_plugins(self):
        "load_plugins should load all modules in directory"
        stats.load_plugins("teststats.plugins")
        mods = sorted([x for x in sys.modules if x.startswith("teststats.plugins.")])
        self.assertEquals(mods,["teststats.plugins.moda", "teststats.plugins.modb"])
    
    def test_dt_to_epoch(self):
        "datetime objects should be converted to epoch seconds correctly"
        # $ SECS=`date +%s`;for x in `rand -N 10`;do S=`expr  $SECS - $x`;python -c "import time;print '%d,%s' %($S,time.ctime($S))";done

        input_lines = """
        1332251643,Tue Mar 20 19:24:03 2012
        1332275119,Wed Mar 21 01:55:19 2012
        1332267061,Tue Mar 20 23:41:01 2012
        1332258295,Tue Mar 20 21:14:55 2012
        1332251367,Tue Mar 20 19:19:27 2012
        1332276562,Wed Mar 21 02:19:22 2012
        1332247835,Tue Mar 20 18:20:35 2012
        1332244476,Tue Mar 20 17:24:36 2012
        1332260095,Tue Mar 20 21:44:55 2012
        1332263354,Tue Mar 20 22:39:14 2012
        """.strip().split("\n")
        inp = {}
        for l in input_lines:
            a,b = l.split(",")
            inp[b] = a
        for datestr in inp:
            dt = datetime.strptime(datestr,"%a %b %d %H:%M:%S %Y")
            self.assertEqual(stats.dt_to_epoch(dt),int(inp[datestr]))
            
        