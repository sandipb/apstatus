
import unittest
import libtest
import apstats.stats
from apstats.parser import LogEntry

class TestStatsReqTypes(unittest.TestCase):
    "Test the functionality of apstats.stats.req_types.StatsReqType"

    def setUp(self):
        if not apstats.stats.plugins_loaded:
            apstats.stats.load_plugins()
        
    def test_stats_req_types(self):
        log_path = libtest.get_test_data_path("l1_1000.log")
        self.assertTrue("req_types" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["req_types"]()
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        calc = stats.get_calculation()

        # $ src/utils/parse_log.py 5 tests/data/l1_1000.log| cut -f 1 -d " " | sort | uniq -c
        # 892 GET
        # 108 HEAD

        expected = [('GET',892,round(892*100/1000,2)),
                    ('HEAD',108,round(108*100/1000,2))]
        self.assertEqual(calc["method_list"], expected)
        self.assertEqual(stats.req_count, 1000)


if __name__ == "__main__":
    unittest.main()
