
import unittest
import libtest
import apstats.stats
from apstats.parser import LogEntry
from datetime import datetime


class TestStatsQPS(unittest.TestCase):
    "Test the functionality of ApStats.stats.qps.StatsQPS"

    def setUp(self):
        if not apstats.stats.plugins_loaded:
            apstats.stats.load_plugins()
        
    def test_statsqps(self):
        log_path = libtest.get_test_data_path("l1_1000.log")
        self.assertTrue("qps" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["qps"]()
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        self.assertEqual(stats.num_queries, 1000)
        # First timestamp = 10/Mar/2012:04:28:02
        # last timestamp = 10/Mar/2012:04:39:02
        begin = datetime(2012,3,10,4,28,2)
        end = datetime(2012,3,10,4,39,2)
        delta = end - begin
        secs = delta.total_seconds()
        qps = stats.num_queries*1.0/secs
        calc = stats.get_calculation()
        self.assertEqual(calc["duration"], secs)
        self.assertEqual(calc["qps"], qps)

    def test_statsqps_smallset(self):
        log_path = libtest.get_test_data_path("l1_5.log")
        self.assertTrue("qps" in apstats.stats.stats_map, str(apstats.stats.stats_map))
        stats = apstats.stats.stats_map["qps"]()
        with open(log_path) as f:
            
            line = f.readline()
            while line:
                stats.process(LogEntry(line))
                line = f.readline()
                
        self.assertEqual(stats.num_queries, 5)
        # First timestamp = 10/Mar/2012:05:23:12 -0800
        # last timestamp = 10/Mar/2012:05:23:17 -0800
        begin = datetime(2012,3,10,5,23,12)
        end = datetime(2012,3,10,5,23,17)
        delta = end - begin
        secs = delta.total_seconds()
        qps = stats.num_queries*1.0/secs
        calc = stats.get_calculation()
        self.assertEqual(calc["duration"], secs)
        self.assertEqual(calc["qps"], qps)

        # with an update
        secs = 10
        qps = stats.num_queries*1.0/secs
        stats.update_duration(secs)
        calc = stats.get_calculation()
        self.assertEqual(calc["duration"], secs)
        self.assertEqual(calc["qps"], qps)

if __name__ == "__main__":
    unittest.main()