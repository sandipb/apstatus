
import os.path
import logging

def get_test_data(filename, return_path=False):
    "Get the file contents of the given file name in the test data directory"
    
    if not filename:
        return ""
    
    this_dir = os.path.abspath(os.path.dirname(__file__))
    file_path = os.path.join(this_dir, "data", filename) 
    if not os.path.exists(file_path) \
       or not os.access(file_path, os.R_OK):
        return ""
    
    if return_path:
        return file_path
    
    try:
        with open(file_path) as f:
            data = f.read()
            return data
    except Exception, e:
        logging.error("Problem opening test data file '%s': %s" % (filename, str(e)))
        return ""
    
def get_test_data_path(filename):
    "Return the full path of the given data filename "
    return get_test_data(filename, return_path=True)

