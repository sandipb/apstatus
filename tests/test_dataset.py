
import unittest
import libtest
from apstats.logreader import LogReader
from apstats.dataset import SimpleDataSet, LimitedDataSet, DataSetFactory
from datetime import datetime

class TestSimpleDataSet(unittest.TestCase):
    "Test the functionality of apstats.dataset.SimpleDataSet"

    def test_simple_dataset(self):
        log_path = libtest.get_test_data_path("logfile1.txt")
        rdr = LogReader(file_name=log_path)
        factory = DataSetFactory(SimpleDataSet, reader=rdr)
        ips_expected = "66.249.67.197 85.25.100.162".split()
        ips_expected.sort()
        ips = []
        num_sets = 0
        for iset in factory:
            num_sets+=1
            for entry in iset:
                ips.append(entry.client_ip)
        ips.sort()
        self.assertEqual(ips, ips_expected)
        self.assertEqual(num_sets, 1)
        
class TestLimitedDataSet(unittest.TestCase):
    "Test the functionality of apstats.dataset.LimitedDataSet"

    def test_limited_dataset(self):
        #$ PYTHONPATH=src ./apstats -d timed_qps:30 tests/data/l1_1000.log
        # 2012-03-10 04:35:00     1.10      33
        # 2012-03-10 04:35:30     1.13      34
        # 2012-03-10 04:36:00     1.77      53

        log_path = libtest.get_test_data_path("l1_1000.log")
        rdr = LogReader(file_name=log_path)
        dt_start = datetime(2012,3,10,4,35)
        dt_end = datetime(2012,3,10,4,36)
        factory = DataSetFactory(LimitedDataSet, reader=rdr, 
                                 time_begin = dt_start,
                                 time_end = dt_end)
        ips_expected = libtest.get_test_data("l1_1000_ips_201203100435.log").strip().split("\n")
        ips = []
        num_sets = 0
        for iset in factory:
            num_sets+=1
            for entry in iset:
                ips.append(entry.client_ip)

        self.maxDiff=None
        self.assertEqual(ips, ips_expected)
        
