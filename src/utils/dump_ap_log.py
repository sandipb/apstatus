#!/usr/bin/env python
'''
Usage: parse_log [column] [file]

- no column will print enumerated form of all columns
- no file will read from stdin
'''

import sys
import shlex

src = sys.stdin

def print_lines(col=None):
    line = src.readline()
    while line:
        cols = shlex.split(line)
        if col is None:
            print list(enumerate(cols))
        else:
            print cols[int(col)]
        line = src.readline()
        
if __name__ == '__main__':
    if len(sys.argv) == 1: # no args
        col=None
    else:
        col = sys.argv[1]
    
    if len(sys.argv) > 2:
        src = open(sys.argv[2]) # no error check
    print_lines(col)
    if src != sys.stdin:
        src.close()
        
        