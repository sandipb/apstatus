'''
Created on 20-Mar-2012

@author: sandipb
'''
from textwrap import dedent
from apstats.stats import Stats

class StatsPrint(Stats):
    """\
    Simply print the log line as-is. You can limit the number by an argument. USE WITH CARE:
    keeps all the log in memory to print out at the end. Can easily make your machines run
    out of memory for large files.
    """
    
    name = "print"
    
    def __init__(self, limit=None, **kwargs):
        Stats.__init__(self, **kwargs)
        self.limit = None
        if limit:
            try:
                self.limit = int(limit)
                if self.limit < 1:
                    raise ValueError()
            except:
                raise ValueError("Bad limit given. Has to be a non-zero int")
            
        self.reset()

    def reset(self):
        self.lines = []
        self.num_lines = 0
    
    def process(self,entry):
        if not self.limit \
           or (self.limit and (self.num_lines < self.limit)):
            self.lines.append(entry.orig_text.rstrip())
            self.num_lines += 1
    
    def get_calculation(self):
                
            return {"lines": self.lines}
            
    def print_report(self):
        calc = self.get_calculation()
        if not calc["lines"]:    
            lines = "N/A (no data)"
        else:
            lines = "\n".join(calc["lines"])

        if not self.limit:
            limit = "No limit"
        else:
            limit = str(self.limit)
            
        report = dedent("""
        =======================
        All Log Lines
        =======================
        Limit: %s
        
        %s
        """) % (limit, lines)
        
        print report.strip()
        
        