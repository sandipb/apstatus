'''
Created on 20-Mar-2012

@author: sandipb
'''
from textwrap import dedent
from apstats.stats import Stats
from datetime import timedelta

class StatsQPS(Stats):
    "Shows a basic query-per-second (QPS) calculation of the entire data."
    
    name = "qps"
    
    def __init__(self, **kwargs):
        Stats.__init__(self, **kwargs)
        self.reset()

    def reset(self):
        self.time_start = None
        self.time_end = None
        self.num_queries = 0
    
    def process(self,entry):
        self.num_queries += 1
        if self.time_start is None:
            self.time_start = entry.time
        self.time_end = entry.time
    
    def get_calculation(self):
        if not self.duration and (self.time_start == self.time_end):
            return {"duration": None, "qps": None}
        else:
            if not self.duration:
                assert(self.time_end > self.time_start)
                delta = (self.time_end - self.time_start).total_seconds()
            else:
                delta = self.duration
                
            return {"duration": delta, 
                    "qps": (self.num_queries * 1.0)/delta}
            
    def print_report(self):
        calc = self.get_calculation()
        if calc["duration"] is None:    
            duration = "N/A (no time range found in data)"
            qps = "N/A"
        else:
            duration = str(timedelta(seconds=calc["duration"]))
            qps = "%.2f" % calc["qps"]

        report = """
        =======================
        QPS
        =======================
        Number of queries: %d
        Duration: %s
        QPS: %s
        """ % (self.num_queries, duration, qps)
        
        print dedent(report).strip()
        
        