'''
Created on 20-Mar-2012

@author: sandipb
'''
from textwrap import dedent
from apstats.stats import Stats

class StatsReqType(Stats):
    "Shows the distribution (%,count) of HTTP request types (GET,POST,etc.) in the entire data."
    
    name = "req_types"
    
    def __init__(self, **args):
        Stats.__init__(self, **args)
        self.reset()

    def reset(self):
        self.reqs = {}
        self.req_count = 0
    
    def process(self,entry):
        self.reqs[entry.method] = self.reqs.get(entry.method,0) + 1
        self.req_count += 1
    
    def get_calculation(self):
        def req_percent(c):
            return round(self.reqs[c] * 100 / self.req_count,2)
        
        if not self.reqs or not self.req_count:
            return {"method_list": None}
        calc = [ (code, self.reqs[code], req_percent(code))
                for code in sorted(self.reqs.keys())]
        return {"method_list": calc}
            
    def print_report(self):
        calc = self.get_calculation()["method_list"]

        if not calc:
            text = "N/A (no data)"    
        else:
            req_list = ["%6s: %d (%3.2f%%)" % (c,n,p) for c,n,p in calc]
            
            text = "\n".join(req_list)

        report = dedent("""
        =======================
        HTTP Request Types
        =======================
        Number of queries: %d
        
        %s
        """) % (self.req_count, text)
        
        print report.strip()
        
        