'''
Created on 20-Mar-2012

@author: sandipb
'''
from textwrap import dedent
from apstats.stats import Stats
from datetime import timedelta

class StatsURLsQPS(Stats):
    """\
    Shows the QPS and count of the various URIs requested by clients. Note, that 
    query string is ignored while comparing URIs. Will show all URIs by default. You
    can give an optional count to restrict the list.
    """
    
    name = "urls_qps"
    
    def __init__(self, max_urls=None, **args):
        Stats.__init__(self, **args)
        self.urls_max = None
        if max_urls:
            try:
                self.urls_max = int(max_urls)
                if self.urls_max < 1:
                    raise ValueError()
            except:
                raise ValueError("Bad URI count given. Has to be a number")
            
        self.reset()

    def reset(self):
        self.time_start = None
        self.time_end = None
        self.url_reqs = {}
    
    def process(self,entry):
        if self.time_start is None:
            self.time_start = entry.time
        self.time_end = entry.time
        # we ignore query strings for these calculations
        # but distinguish /url from /url/
        self.url_reqs[entry.path] = self.url_reqs.get(entry.path,0) + 1 
    
    def get_calculation(self):
        
        if not self.duration and (self.time_start == self.time_end):
            return {"duration": None, "url_list": None}
        else:
            if not self.duration:
                assert(self.time_end > self.time_start)
                delta = (self.time_end - self.time_start).total_seconds()
            else:
                delta = self.duration
                
            url_list = []
            for url in self.url_reqs.keys():
                count = self.url_reqs[url]
                qps = count*1.0/delta
                url_list.append([url,count,qps])
            url_list.sort(key=lambda x:x[1], reverse=True)
            if self.urls_max:
                url_list = url_list[:self.urls_max]
                    
            return {"duration": delta, 
                    "url_list": url_list}
            
    def print_report(self):
        calc = self.get_calculation()
        if calc["duration"] is None:    
            duration = "N/A (no time range found in data)"
            url_list_text = "N/A"
        else:
            duration = "%s (%d seconds)" % (str(timedelta(seconds=calc["duration"])),
                                            calc["duration"])
            url_list_text = "\n".join(["%3d. %7.1f %7d  %s" % (n+1,u[2],u[1],u[0]) for n,u in enumerate(calc["url_list"])])
        
        if self.urls_max:
            limit_text = "Top %d" % self.urls_max
        else:
            limit_text = "No limit"
            
        report = dedent("""
        =======================
        QPS of URIs
        =======================
        Duration: %s
        Limit: %s
        
           # %7s %7s  URI
        ~~~~~~~~~~~~~~~~~~~~~~~~~
        %s        
        """) % (duration, limit_text, "QPS","Count",url_list_text)
        
        print report.strip()
        
        