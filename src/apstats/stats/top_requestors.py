'''
Created on 20-Mar-2012

@author: sandipb
'''
from textwrap import dedent
from apstats.stats import Stats

class StatsTopRequestors(Stats):
    "Shows the top IPs which have requested pages from the server and their count. "
    "By default, shows top 10 IPs, but you can change that with an argument"
    
    name = "top_ips"
    
    def __init__(self, limit=10, **args):
        Stats.__init__(self, **args)
        try:
            self.ip_max = int(limit)
            if self.ip_max < 1:
                raise ValueError()
        except:
            raise ValueError("Bad IP limit given. Has to be a non-zero int")
        
        self.reset()

    def reset(self):
        self.reqs = {}
    
    def process(self,entry):
        self.reqs[entry.client_ip] = self.reqs.get(entry.client_ip,0) + 1
    
    def get_calculation(self):
        def ip_sort(a,b):
            if self.reqs[a] != self.reqs[b]:
                return cmp(self.reqs[a],self.reqs[b])
            else:
                return cmp(a,b)
            
        return {"top": 
                sorted(self.reqs.keys(), 
#                       key=lambda x:self.reqs[x],
                       cmp=ip_sort,
                       reverse=True)[:self.ip_max]}
            
    def print_report(self):
        calc = self.get_calculation()
        if not calc["top"]:
            text = "N/A (no data)"    
        else:
            ip_list = ["%2d. %s (%d)" % (n+1,i, self.reqs[i]) 
                        for n,i in enumerate(calc["top"])]
            
            text = "\n".join(ip_list)
        limit_text = "top %d IPs" % self.ip_max
        report = dedent("""
        =======================
        Top Requestors
        =======================
        Limit: %s
        
        %s
        """) % (limit_text,text)
        
        print report.strip()
        
        