'''
Created on 20-Mar-2012

@author: sandipb
'''
from textwrap import dedent
from apstats.stats import Stats

class StatsErrorList(Stats):
    "List of 4xx and 5xx errors and their counts."
    
    name = "errors"
    
    def __init__(self,**kwargs):
        Stats.__init__(self, **kwargs)
        self.reset()

    def reset(self):
        self.reqs = {}
    
    def process(self,entry):
        if entry.status >= 400:
            status_str = str(entry.status) 
            self.reqs[status_str] = self.reqs.get(status_str,0) + 1
    
    def get_calculation(self):
            
        return {"errors": sorted(self.reqs.keys())}
            
    def print_report(self):
        calc = self.get_calculation()
        if not calc["errors"]:
            text = "N/A (no data)"    
        else:
            err_list = ["Error %s = %d" % (s, self.reqs[s]) 
                        for s in calc["errors"]]
            
            text = "\n".join(err_list)

        report = dedent("""
        =======================
        List of Request Errors
        =======================
        %s
        """) % text
        
        print report.strip()
        
        