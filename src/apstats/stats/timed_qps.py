'''
Two scenarios exist:
- full file parse
- continuous parse

In full file parse: workflow is generate interval data, print
In continous parse, the workflow will be: generate, print, reset, generate,print, reset,...
'''
from textwrap import dedent
from apstats.stats import Stats, dt_to_epoch
from datetime import timedelta, datetime


class StatsTimedQPS(Stats):
    """
    
    Shows the QPS values over fixed intervals of time (default 300 seconds). 
    You can change the interval of time by passing it as a parameter (in seconds)
    to this stats function. e.g. '--display timed_qps:600'. It will also show the 
    avg QPS over the entire time, and minimum/maximum QPS values over all the intervals.
    
    """
    
    name = "timed_qps"
    
    def __init__(self, time_period="300",**kwargs):
        Stats.__init__(self, **kwargs)
        self.reset()
        try:
            self.time_period = int(time_period)
        except Exception:
            raise ValueError("Bad interval value '%s'. Should be in seconds" % time_period)

    def reset(self):
        self.num_queries = 0
        self.record = {}
    
    def get_zone(self,dt):
        "Return the zone start value in which the dt value would lie"
        ts = dt_to_epoch(dt)
        ts_start = ts - (ts % self.time_period)
        return datetime.fromtimestamp(ts_start)
        
    def process(self,entry):
        self.num_queries += 1
        zone = self.get_zone(entry.time)
        self.record[zone] = self.record.get(zone,0) + 1
        
    def get_calculation(self):
        if not self.record or not self.num_queries:
            return {"zone_list": None, "total_queries": None,
                    "min_qps": None, "max_qps": None, "avg_qps": None }


        cur_time = datetime.now()
        zones = sorted(self.record.keys())
        if self.duration:
            time_start = self.get_zone(cur_time - timedelta(seconds=self.duration))
            time_end = self.get_zone(cur_time)

            # Fix any syncing issues
            if time_start > zones[0]:
                time_start = zones[0]
                
            if time_end < zones[-1]:
                time_end = zones[-1]
        else:
            time_start = zones[0]
            time_end = zones[-1]
            
        zone_list, minq, maxq, avgq = self.get_zone_stats(time_start,time_end)
        
        return {"zone_list": zone_list, 
                "total_queries": self.num_queries,
                "min_qps": minq,
                "max_qps": maxq,
                "avg_qps": avgq}
        
    def get_zone_stats(self,dt_start, dt_end):
        """
        Return stats for every zone between dt_start and dt_end, even though they 
        might not have an entry
        """
        dt = dt_start
        max_qps = None
        min_qps = None
        total_duration = (dt_end-dt_start).total_seconds() + self.time_period
        avg_qps = self.num_queries * 1.0/ total_duration
        
        zlist = []
        while dt <= dt_end:
            if not (dt in self.record):
                zlist.append([dt,0,0])
            else:
                nqueries = self.record[dt]
                qps = nqueries * 1.0 / self.time_period
                zlist.append([dt, nqueries, qps ])
                if min_qps is None: # first zone
                    min_qps = max_qps = qps
                    
                if qps > max_qps:
                    max_qps = qps
                if qps < min_qps:
                    min_qps = qps
                
            dt += timedelta(seconds=self.time_period)
        return [zlist, min_qps, max_qps, avg_qps]
        
                
    def print_report(self):
        calc = self.get_calculation()
        zone_list = calc["zone_list"]

        if not zone_list: # handle both None and []    
            duration = "N/A (no time range found in data)"
            zone_list_text = "N/A"
            calc["avg_qps"] = calc["min_qps"] = calc["max_qps"] = 0.0   
        else:
            dt_begin = zone_list[0][0]
            dt_end = zone_list[-1][0] + timedelta(seconds=(self.time_period - 1))
            duration = "%s - %s" % (dt_begin.strftime("%Y-%m-%d %H:%M:%S"),
                                     dt_end.strftime("%Y-%m-%d %H:%M:%S"))
            zlists = ["%s %8.2f   %5d" % (t.strftime("%Y-%m-%d %H:%M:%S"),q, c) for t,c,q in zone_list ]
            zone_list_text = "\n".join(zlists)

        report = dedent("""
        =======================
        QPS per interval
        =======================
        Number of queries: %d
        Duration: %s
        Time interval: %d seconds 
        
        Interval Start           QPS   Count
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        %s
        
        QPS  Avg: %4.2f   Min: %4.2f   Max: %4.2f
        """) % (self.num_queries, duration, self.time_period, zone_list_text, 
                calc["avg_qps"], calc["min_qps"], calc["max_qps"])
        
        print dedent(report).strip()
        
        
