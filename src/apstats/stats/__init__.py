
import logging
from os.path import basename, dirname, abspath, splitext
from os.path import join as osjoin
import glob
import sys
import time
from textwrap import dedent, TextWrapper

logger = logging.getLogger("apstats.stats")
if not logger.handlers:
    logger = logging
stats_map = {}
plugins_loaded = False

class Stats(object):
    "Crude plugin system based on http://effbot.org/zone/metaclass-plugins.htm"
    class __metaclass__(type):        
        def __init__(cls, name, bases, loc_dict):
            type.__init__(cls, name, bases, loc_dict)
            if name != 'Stats':
                if not hasattr(cls,'name'):
                    logger.warning("Ignoring stats class '%s' as it doesn't have a name" % name)
                else:
                    stats_map[cls.name] = cls

    def __init__(self,duration=None):
        self.duration = duration
        
    def update_duration(self,duration):
        self.duration = duration
                    
def load_plugins(pkg_name="apstats.stats"):
    "Load all module file in the given package"
    global plugins_loaded
    
    if plugins_loaded:
        logger.warning("Plugins already loaded. Not reloading")
        return
    
    if not pkg_name in sys.modules:
        try:
            __import__(pkg_name)
        except:
            logger.error("Could not load package '%s'" % pkg_name)
            return 
    pkg = sys.modules[pkg_name]
    mod_list = [ splitext(basename(x))[0] for x in glob.glob(osjoin(dirname(pkg.__file__),"*.py")) 
                    if basename(x)!="__init__.py" ]
    logger.debug("importing modules: " + str(mod_list))
    for mod_name in mod_list:
        __import__("%s.%s" % (pkg_name,mod_name))
    plugins_loaded = True

def dt_to_epoch(dt):
    return int(time.mktime(dt.timetuple()))

def print_functions():
    if not stats_map:
        print "No stats functions available"
    else:
        report_temp = dedent("""
        List of Available aggregation functions
        ---------------------------------------
        %s
        """)
        text = []
        doc_wrapper=TextWrapper(initial_indent="   ", 
                                subsequent_indent="   ")
        for seq,name in enumerate(sorted(stats_map.keys())):
            doc = dedent(stats_map[name].__doc__)
            #print "[%s]" % doc
            paras = [ "\n".join(doc_wrapper.wrap(x))  for x in doc.split("\n\n")]
            sub_text = dedent("""
            %d. %s            
            %s
            """) % (seq+1, name, 
                    "\n".join(paras))
            text.append(sub_text)
        text = "".join(text)
        print report_temp % text