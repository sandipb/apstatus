'''
Created on 20-Mar-2012

@author: sandipb
'''
from textwrap import dedent
from apstats.stats import Stats

class StatsResType(Stats):
    "Shows the distribution (%,count) of HTTP response codes (200,302,etc.) in the entire data."
    
    name = "res_types"
    
    def __init__(self, **args):
        Stats.__init__(self, **args)
        self.reset()

    def reset(self):
        self.resps = {}
        self.resp_count = 0
    
    def process(self,entry):
        self.resps[str(entry.status)] = self.resps.get(str(entry.status),0) + 1
        self.resp_count += 1
    
    def get_calculation(self):
        def resp_percent(c):
            return round(self.resps[c] * 100 / self.resp_count,2)
        
        if not self.resps or not self.resp_count:
            return {"response_list": None}
        calc = [ (code, self.resps[code], resp_percent(code))
                for code in sorted(self.resps.keys())]
        return {"response_list": calc}
            
    def print_report(self):
        calc = self.get_calculation()["response_list"]

        if not calc:
            text = "N/A (no data)"    
        else:
            resp_list = ["HTTP %3s: %d (%3.2f%%)" % (c,n,p) for c,n,p in calc]
            
            text = "\n".join(resp_list)

        report = dedent("""
        =======================
        HTTP Response Types
        =======================
        Number of queries: %d
        
        %s
        """) % (self.resp_count, text)
        
        print report.strip()
        
        