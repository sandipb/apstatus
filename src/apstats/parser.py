"""
Log file parsing logic
"""

import shlex
import logging
import re
from datetime import datetime

logging.getLogger("apstats.parser")

class ParserException(Exception): pass

#naive match. We are not trying to catch invalid IPs :)
re_pattern_ip = re.compile(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}')

class LogEntry(object):
    """
    Represents a single line in the log file.
    
    This class will parse a CLF line and keep the parsed representation of the data
    within.
    
    Combined Log Format (CLF)
    Ref: http://httpd.apache.org/docs/1.3/logs.html#combined
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" combined
    
    %h = IP address of the client
    %l = identd login name
    %u = HTTP auth username
    %t = Time server *finished* processing the request. 
         Format:
         [day/month/year:hour:minute:second zone]
            day = 2*digit
            month = 3*letter
            year = 4*digit
            hour = 2*digit
            minute = 2*digit
            second = 2*digit
            zone = (`+' | `-') 4*digit
    %r = Request line from client
         in the form: %m %U%q %H
             %m = http method
             %U = URI requested
             %q = query string
             %H = protocol used
    %>s = status code sent by server back to client
    %b = size of the object returned to client, not including headers
    %{Referer}i = Referer
    %{User-agent}i = user agent
    """
    
    def __init__(self, line):
        """
        Parse the line and set internal variables for each component of the entry
        
        Expect an entry to be of the form:
         157.55.18.23 - - [11/Mar/2012:00:24:43 -0800] "GET /2012/01/19/pratap-bhanu-mehta-on-state-censorship-in-india/ HTTP/1.1" 200 5468 "-" "Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)" 
        """
        self.logger = logging.getLogger("apstats.parser.logentry")
        
        self.orig_text = line

        cols = shlex.split(line)
        
        # join the split timestamp column 
        cols = cols[:3] + [" ".join(cols[3:5])] + cols[5:]
        
        if not len(cols) == 9:
            raise ParserException("Invalid log entry line. Fields != 9")
        
        
        self.client_ip = None
        # XXX: In the absence of sample format, am assuming that host lookups have not been
        # turned on, as it shouldn't be, and that the first field is always ip addresses

                 
        if not re_pattern_ip.match(cols[0]): 
            if cols[0].count(":") < 2: #ugly hack to let ipv6 addresses escape validation
                raise ParserException("Invalid IP: " + cols[0])
        
        self.client_ip = cols[0]
            
        self.client_ident = cols[1]
        self.client_auth_name = cols[2]
        self.time = self.parse_time(cols[3])

        self.request = cols[4]
        
        self.method, self.uri,self.protocol = self.parse_request(self.request)

        self.path, self.qs = self.parse_uri(self.uri)
        
        self.status = self.parse_status(cols[5])

        try:         
            self.size = int(cols[6])
        except ValueError, e:
            if cols[6] == "-": # happens with HEAD
                self.size = 0
            else:
                raise ParserException("Invalid size value '%s': %s" % (cols[6],e))

        self.referer = cols[7]
        self.ua = cols[8]
         
    def parse_time(self, text):
        """
        Parse the date time text in log file and return a datetime object
        
        input format = [11/Mar/2012:00:24:43 -0800]
        
        We strip off the timezone text as we don't need timezone aware calculations
        """
    
        fmt = "[%d/%b/%Y:%H:%M:%S "
        
        try:
            dt = datetime.strptime(text[:-6],fmt)
        except ValueError, e:
            raise ParserException("Invalid CLF date format - '%s': %s" % (text, e))
        
        return dt
    
        
        
    def parse_request(self, text):
        """
        Parse the request component of the CLF log line and return a tuple of(method,uri,protocol)
        
        Input is of the format:
        GET /2012/01/19/pratap-bhanu-mehta-on-state-censorship-in-india/ HTTP/1.1
        """
        cols = text.split()
        if len(cols) != 3:
            raise ParserException("Invalid request column in log entry line. Fields != 3")
        
        return [cols[0].upper(), cols[1], cols[2].upper()]
    
    def parse_status(self, text):
        """
        Parse the status text and do a sanity check before returning the status as an int
        
        Very naive sanity checking done here.
        
        Accepted status codes are given in http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
        """
        try:
            st = int(text)
            if st < 100 or st > 505:
                raise ValueError("Invalid status code value: " + text)
        except ValueError, e:
            raise ParserException("Invalid status value '%s': %s" % (text,e))
        
        return st
        
    # Not a priority
    def parse_uri(self, uri):
        """
        Parse the request URI and return the path and the query string
        """
        comps = uri.split('?',1)
        if len(comps) != 2:
            return [uri,None]
        return comps
    
