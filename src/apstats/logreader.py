'''
Provide data sets
'''

import sys
import os
import logging

class LogReaderException(Exception):
    pass

class LogReader(object):
    "Read a file_handle and return a suitable data set manager"
    
    def __init__(self, file_handle=None, file_name=None, stdin=False):

        self.file_handle = file_handle
        self.file_name = file_name
        self.stdin = stdin
        self.logger = logging.getLogger("apstats.logreader")
        
        if not (file_handle or file_name or stdin):
            raise LogReaderException("No input option specified in init")
        
        if (file_handle and (file_name or stdin))\
           or (file_name and stdin):
            raise LogReaderException("More than one input specified")
        
        if file_name:  
            try:
                self.file_handle = open(file_name)
            except Exception, e:
                raise LogReaderException("Error opening given log file '%s': %s" % (file_name, e))
        
        if stdin:
            self.file_handle = sys.stdin

    def readline(self):
        line = None
        while not line:
            line = self.file_handle.readline()
            if not line: #feof
                return line 
            line = line.strip()
            if line: # blank line
                return line

                    
    def __del__(self):
        "Only close the filehandle when we are the person who opened it"
        if self.file_name and self.file_handle:
            self.file_handle.close()

class TailLogReader(LogReader):
    
    def __init__(self,*args, **kwargs):
        LogReader.__init__(self,*args, **kwargs)
        self.file_handle.seek(0, os.SEEK_END)
        self.logger = logging.getLogger("apstats.taillogreader")

    def readline(self):
        line = None
        while not line:
            line = self.file_handle.readline()
            if not line: #feof
                return line 
            if not line.endswith("\n"): # partial line, rewind and say it is feof
                self.file_handle.seek(-len(line),os.SEEK_CUR)
                return ""
            line = line.strip()
            if line: # skip blank lines
                return line

                    