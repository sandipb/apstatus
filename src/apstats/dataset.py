'''
Created on 20-Mar-2012

@author: sandipb
'''
from apstats.parser import LogEntry, ParserException
import logging
import time
from datetime import datetime

logger = logging.getLogger("apstats.dataset")
# fct = DataSetFactory(SimpleDataSet,a=b)
# for set in fct:
#     for entry in set

graceful_termination_request = False
termination_in_progress = False

class DataSetException(Exception): pass

class DataSetFactory(object):
    """
    Factory for invoking new datasets.
    
    Normally it will keep creating new sets of the given type forever. But
    if the given dataset class has a max_instances class variable, it will
    never create instances more than that. 
    """

    def __init__(self, data_set, **kwargs):
        self.options = kwargs
        self.classref = data_set
        self.logger = logging.getLogger("apstats.dataset.factory")
        self.num_instances = 0

    def __iter__(self):
        return self

    def next(self): #@ReservedAssignment
        cls = self.classref
        if not cls.max_instances \
            or (cls.max_instances and self.num_instances < cls.max_instances):
            obj = cls(**self.options)
            self.num_instances += 1 
            obj.factory = self
            return obj
        else:
            raise StopIteration
        
        
class DataSet(object):
    
    #: How many instances of this set can be created for a logfile? If None, means no limit
    max_instances = None
    inform_interruption = False
    override_time_duration = False
    
    def __init__(self, **kwargs):
        if not "reader" in kwargs:
            raise DataSetException("No 'reader' passed on to the constructor")
        self.reader = kwargs["reader"]
        
            
class SimpleDataSet(DataSet):
    
    max_instances = 1
    
    def __init__(self,**kwargs):
        DataSet.__init__(self,**kwargs)
        self.line_num = 0
            
    def __iter__(self):
        return self
    
    def next(self): #@ReservedAssignment
        entry = None
        while entry is None: # Read till a valid line
            line = self.reader.readline()
            if not line:
                raise StopIteration
            
            self.line_num +=1
        
            try:
                entry = LogEntry(line)
            except ParserException, e:
                self.logger.error("Couldn't parse line number %d ('%s ...'): %s" 
                                  % (self.line_num, line[:10], e))

        return entry
    
class LimitedDataSet(DataSet):
    
    max_instances = 1
    
    def __init__(self, time_begin, time_end, **kwargs):
        DataSet.__init__(self,**kwargs)
        self.run_started = False
        self.time_begin = time_begin
        self.time_end = time_end
        logger.debug("Will capture between %s and %s" % (str(time_begin),str(time_end)))
            
    def __iter__(self):
        return self
    
    def next(self): #@ReservedAssignment
        entry = None
        while entry is None: # Read till a valid line
            line = self.reader.readline()
            if not line:
                raise StopIteration
            
            try:
                entry = LogEntry(line)
            except ParserException, e:
                self.logger.error("Couldn't parse line  ('%s ...'): %s" 
                                  % (line[:20], e))
            
            if self.run_started:
                # Handle the weird out-of-order case
                if entry.time < self.time_begin:
                    entry = None
                    continue

                if entry.time < self.time_end:
                    return entry
                else:
                    raise StopIteration # consumes one Entry
            else:
                if entry.time < self.time_begin:
                    entry = None
                    continue
                else:
                    self.run_started = True
                    logger.info("First entry in zone: " + str(entry.time))
                    return entry
                    
class ContinuousTimedDataSet(DataSet):
    
    max_instances = None
    override_time_duration = True
    
    def __init__(self, duration, **kwargs):
        global graceful_termination_request
        
        DataSet.__init__(self,**kwargs)
        self.duration = self.actual_duration = duration
        logger.debug("Capture interval: Every %s seconds" % str(duration))
        self.begin_time = int(time.time())
        self.end_time = self.begin_time + self.duration
        
        graceful_termination_request = True
        print "Duration: %s - %s \n" \
            % (datetime.fromtimestamp(self.begin_time).strftime("%Y-%m-%d %H:%M:%S"),
               datetime.fromtimestamp(self.end_time).strftime("%Y-%m-%d %H:%M:%S")) 
            
    def __iter__(self):
        return self
    
    def next(self): #@ReservedAssignment
        # End only if said number of lines raid
        now = int(time.time())
        if (now >= self.end_time)  or termination_in_progress:
            if (now == self.begin_time):
                self.actual_duration = 1
            else:
                self.actual_duration = (now - self.begin_time)
            raise StopIteration
        
        entry = None
        while entry is None: # Read till a valid line
            
            line = ""
            while not line:
                line = self.reader.readline()
                if not line:
                    time.sleep(0.5) # sleep briefly
                    now = int(time.time())
                    if (now >= self.end_time) or termination_in_progress:
                        if (now == self.begin_time):
                            self.actual_duration = 1
                        else:
                            self.actual_duration = (now - self.begin_time)
                        raise StopIteration
                    
            try:
                entry = LogEntry(line)
            except ParserException, e:
                self.logger.error("Couldn't parse line  ('%s ...'): %s" 
                                  % (line[:20], e))
            
        return entry
    
    
class ContinuousLinesDataSet(DataSet):
    
    max_instances = None
    
    def __init__(self, duration, **kwargs):
        global graceful_termination_request
        
        DataSet.__init__(self,**kwargs)
        self.duration = duration
        logger.debug("Capture interval: Every %s lines" % str(duration))
        self.lines_read = 0
        graceful_termination_request = True
            
    def __iter__(self):
        return self
    
    def next(self): #@ReservedAssignment
        # End only if said number of lines raid
        if (self.lines_read == self.duration) or termination_in_progress:
            raise StopIteration
        
        entry = None
        while entry is None: # Read till a valid line
            
            line = ""
            while not line:
                line = self.reader.readline()
                if not line:
                    time.sleep(0.5) # sleep briefly
                    if termination_in_progress:
                        raise StopIteration
            
            try:
                entry = LogEntry(line)
            except ParserException, e:
                self.logger.error("Couldn't parse line number %d ('%s ...'): %s" 
                                  % (self.line_num, line[:10], e))
            
        self.lines_read += 1    
        return entry
    
