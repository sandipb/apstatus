=========
CHANGELOG
=========

.. contents::

version 1.0.4
-------------

Doc fix.

- Add testing instructions to the README
- Add a changelog
- Forgot to add the data files to the test suit. Fixed.

version 1.0.3
-------------

Add tests to the source archive. Now you can use ``python setup.py nosetests``
or ``python setup.py test`` to run the unittests.

version 1.0.2
-------------

Don't allow continuous display to read from stdin. Doc updated.

version 1.0.1
-------------

Fix integer division in qps stats.
 