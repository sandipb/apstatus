===================
Problem Description
===================

.. contents::

Required
--------

Given an apache log file (provided) formulate a script that can take arguments
to do any (or all) the following:

1. Produce the minimum, maximum, and average QPS for a configurable time
   segment. For example if asked for the these numbers for a 5-minute time
   period, print the QPS numbers for 00:00 to 00:04, 00:05 to 00:19, 00:10 to
   00:14, and so on.

2. Produce a list of the URLs called and the average QPS of those URLs.

3. Produce a list of the top 10 requestors.

4. Produce a list of errors (4xx and 5xx status codes) by URL and their call
   count

5. Produce a percentage report of request types, for example GET, PUT, etc. So
   if there were 100 requests and 95 were GETs and 5 were PUTs, your report
   would say::

        GET: 95%
        PUT: 5%

   Or something similar.

6. Produce a percentage report of response types (200 vs 404 vs 500, etc).
   Format it similar to the above report.

Allow for all previous operations to occur for a provided time period. For
example, allow the user to specify 01:00 to 03:00 and only report for those
times.

Bonus
-----

Allow for the script to work on a constantly updating file. The user would
specify that a report should be generated every N minutes. For example, if a
user said the 5 minutes, the operation listed above would print its report over
the last 5 minutes, every 5 minutes.

.. note: Please DO NOT re-read the entire file every N minutes to provide this
         information.

.. vim:tw=80:ts=4:sw=4:fo+=n:ai:et
