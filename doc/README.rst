=================================
apstats - The Apache log analyzer
=================================

.. contents::

--------
Overview
--------

*apstats* is an extensible apache log analyzer. 

It can run various aggregate statistical functions over a given Apache log in
CLF (Combined Log Format). The analysis can be done over a historical log, or
even on a log file being updated in real-time. Analysis can be done over an
entire log file, or it can be done over a section of the log file limited by a
time duration.

----------
Installing
----------

Creating the distributable package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can create a standard distributable python package from the code by running
the following in the top level directory ::

    python setup.py sdist

This will create a ``apstats-x.y.z.tar.gz`` archive under ``dist/`` which you can then use
anywhere.

Installing the package
~~~~~~~~~~~~~~~~~~~~~~

The package can be installed on any machine using the ``pip`` python installer.
To test the package, I would recommend you to install it inside a `virtualenv`_.

::

    $ mkvirtualenv testap
    (testap) $ pip install dist/apstats-1.0.0.tar.gz 

.. _virtualenv: http://www.virtualenv.org/ 

Running the test suite
~~~~~~~~~~~~~~~~~~~~~~

You should run the unit-tests of the package on the target system before installing on that. 
This will ensure that the system python binary can run the code in the package. The tool could
be using features of python not supported by the system python binary.

::

   $ tar xvzf apstats-1.0.4.tar.gz 
   apstats-1.0.4/
   apstats-1.0.4/tests/
   apstats-1.0.4/tests/test_stats_qps.pyc
   apstats-1.0.4/tests/test_stats.py
   apstats-1.0.4/tests/test_stats_print.pyc
   apstats-1.0.4/tests/test_stats_top_requestors.pyc
   apstats-1.0.4/tests/test_stats_print.py
   ...
   
   $ cd apstats-1.0.4/
   
   $ python setup.py test
   ...
   test_dt_to_epoch (tests.test_stats.TestStats)
   datetime objects should be converted to epoch seconds correctly ... ok
   test_load_plugins (tests.test_stats.TestStats)
   load_plugins should load all modules in directory ... ok
   test_statsprint (tests.test_stats_print.TestStatsPrint) ... ok
   test_stats_error_list (tests.test_stats_urls_qps.TestStatsURLsQPS) ... ok
   test_stats_error_list (tests.test_stats_error_list.TestStatsErrorList) ... ok
   ...


-----
Usage
-----


Synopsis
~~~~~~~~

The simplest possible usage of the tool is demonstrated below ::

    $ apstats -d qps tests/data/snet_20120312.log 

    =======================
    QPS
    =======================
    Number of queries: 6536
    Duration: 1 day, 23:50:53
    QPS: 0.04

Providing the log data
~~~~~~~~~~~~~~~~~~~~~~

*apstats* can work on a given log file, or can take piped input from standard
input. 

In above example, the log file ``tests/data/snet_20120312.log`` was provided for
analysis. If no log file is provided or the log file is given to be as a hyphen
(``-``), then the log data will be read from stdin.

Requesting the statistical functions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Also, in the example above, ``-d qps`` is provided to tell the tool to run the
``qps`` function on the data. 

There are several such statistical functions available with the tool. To see a
list of the available functions and their description, run the tool with the
``--list-functions`` option. Here is the current list of available functions ::

    $ apstats --list-functions

    List of Available aggregation functions
    ---------------------------------------

    1. errors            
       List of 4xx and 5xx errors and their counts.

    2. print            
       Simply print the log line as-is. You can limit the number by an
       argument. USE WITH CARE: keeps all the log in memory to print out
       at the end. Can easily make your machines run out of memory for
       large files.

    3. qps            
       Shows a basic query-per-second (QPS) calculation of the entire
       data.

    4. req_types            
       Shows the distribution (%,count) of HTTP request types
       (GET,POST,etc.) in the entire data.

    5. res_types            
       Shows the distribution (%,count) of HTTP response codes
       (200,302,etc.) in the entire data.

    6. timed_qps            

       Shows the QPS values over fixed intervals of time (default 300
       seconds).  You can change the interval of time by passing it as a
       parameter (in seconds) to this stats function. e.g. '--display
       timed_qps:600'. It will also show the  avg QPS over the entire
       time, and minimum/maximum QPS values over all the intervals.


    7. top_ips            
       Shows the top IPs which have requested pages from the server and
       their count.

    8. urls_qps            
       Shows the QPS and count of the various URIs requested by clients.
       Note, that  query string is ignored while comparing URIs. Will show
       all URIs by default. You can give an optional count to restrict the
       list.
 
You can ask for multiple functions to be run over the data by separating the
function names with a comma (``,``). e.g.::

    $ apstats -d qps,top_ips tests/data/snet_20120312.log 
    =======================
    QPS
    =======================
    Number of queries: 6536
    Duration: 1 day, 23:50:53
    QPS: 0.04

    =======================
    Top Requestors
    =======================
    Limit: top 10 IPs

     1. 38.111.147.84 (477)
     2. 66.249.67.197 (358)
     3. 124.115.1.61 (264)
     4. 67.205.56.98 (219)
     5. 217.172.180.18 (117)
     6. 199.21.99.69 (87)
     7. 72.30.161.236 (74)
     8. 119.205.211.103 (66)
     9. 110.34.171.18 (56)
    10. 96.228.97.189 (54)
 
You can customize the output of some of these functions by appending an option
to their name prefixed by a colon (``:``). e.g. ::

    $ apstats -d qps,top_ips:5 tests/data/snet_20120312.log 
    =======================
    QPS
    =======================
    Number of queries: 6536
    Duration: 1 day, 23:50:53
    QPS: 0.04

    =======================
    Top Requestors
    =======================
    Limit: top 5 IPs

     1. 38.111.147.84 (477)
     2. 66.249.67.197 (358)
     3. 124.115.1.61 (264)
     4. 67.205.56.98 (219)
     5. 217.172.180.18 (117)

You can ask for all the available functions to be applied to the data by using
the function name ``all``. e.g. ``apstats -d all /var/log/apache/access``. If
you want to customize some of the functions even while running all of them,
simply add them to the function list as always. e.g. 
``apstats -d all,top_ips:5 /var/log/apache/access``.

Limiting the analysis to a section of the log file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can run the analysis on a section of the log file by using the ``--limit``
or the ``-l`` option.

::

    $ apstats -d timed_qps -l 201203122240-201203122305 tests/data/snet_20120312.log 
    =======================
    QPS per interval
    =======================
    Number of queries: 111
    Duration: 2012-03-12 22:40:00 - 2012-03-12 23:04:59
    Time interval: 300 seconds 

    Interval Start           QPS   Count
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    2012-03-12 22:40:00     0.02       6
    2012-03-12 22:45:00     0.07      20
    2012-03-12 22:50:00     0.09      28
    2012-03-12 22:55:00     0.12      36
    2012-03-12 23:00:00     0.07      21

    QPS  Avg: 0.07   Min: 0.02   Max: 0.12

The section of the log file to be analyzed is mentioned as a time range of the
format ``FROM-TO``, where ``FROM`` and ``TO`` need to be in the form
``[[[YYYY]MM]DD]HHMM``. Only the hours and minutes in ``FROM`` and ``TO`` is mandatory. Any
part left out in ``FROM`` will be taken from *today*. Any part left out in
``TO`` is taken from ``FROM``. Therefore, you can shorten the command shown
above to ::

    $ apstats -d timed_qps -l 201203122240-2305 tests/data/snet_20120312.log 

You can leave out ``TO`` entirely. In that case, all logs entries till present
will be shown.

.. note:: The ``--limit`` option is incompatible with the ``--continuous``
          option mentioned in the next section.

Running the analysis continuously
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The tool can be used to run on the log file continuously, displaying its
analysis on regular intervals on the logs generated till then. The intervals can
be based either on regular time intervals or even after fixed number of log
lines.

To run the tool in continuous analysis mode, use the ``--continuous`` or ``-c``
option. By default the tool will shows the analysis every *10 seconds*. You can
change this interval using the ``--cont-interval`` or ``-n`` option. The
parameter to this option can be of the following types:

1. If just a number is given, it would be taken to be in seconds.

2. If the number is suffixed with a *scale* alphabet, then it is calculated
   thus:

    * If the *scale* alphabet is ``s``, then the number is taken to be seconds.
    * If the *scale* alphabet is ``m`` or ``h``, then the number is taken to be
      in minutes or hours, respectively.
    * If the *scale* alphabet is ``l``, then the number is taken to be in number
      of lines.

Examples ::

    # Run the analysis continuously on the log file and display analysis
    # every 30 seconds

    $ apstats -d qps -c -n 30 tests/data/snet_20120312.log


    # Run the analysis continuously on the log file and display analysis
    # every 5 minutes

    $ apstats -d qps -c -n 5m tests/data/snet_20120312.log


    # Run the analysis continuously on the log file and display analysis
    # every 10 lines

    $ apstats -d qps -c -n 10l tests/data/snet_20120312.log


.. note:: The ``--continuous`` option is incompatible with the ``--limit``
          option mentioned in the previous section. It will also not work when reading
          from piped data via stdin.

-----------------
Development Notes
-----------------

Software Components
~~~~~~~~~~~~~~~~~~~

Here are the entities involved in the work :

1. The master script ``apstats`` which parses the arguments and calls the rest
   of the components accordingly.

2. Log readers: These read through the log files, clean and parse each line, and
   then present them to the functions one by one. There are two kind of log
   readers - one which reads a log file from top to bottom
   (``apstats.logreader.LogReader``), and another which
   reads log files in a *tail* mode (``apstats.logreader.TailLogReader``).

3. Data sets: These package a set of log entries for the functions to analyze at
   a time.  There are presently four kind of data sets:
   
   - Provides the whole file content at once (``apstats.dataset.SimpleDataSet``)

   - Provides a time limited section of the log
     (``apstats.dataset.LimitedDataSet``)

   - Provides a continuous real-time part of the log file, where each interval
     is separated by a fixed time interval
     (``apstats.dataset.ContinuousTimedDataSet``)

   - Provides a continuous real-time part of the log file, where each interval
     is separated by a fixed number of lines
     (``apstats.dataset.ContinuousLinesDataSet``)

4. Finally, the statistical functions themselves. These functions are provided
   using a plugin mechanism, which allows the user to just add a corresponding
   python file in the ``apstats.stats`` package, and it will automatically be
   available to users for use. You can take a look at the currently provided
   modules in ``apstats.stats`` package to see the simple API to write your own
   functions.

Log File Format
~~~~~~~~~~~~~~~

Each log line is represented by the ``LogEntry`` class which can handle the *Combined
Log Format (CLF)*. But this class can be extended to handle any log format. Of
course, the data in the log format needs to be matched with the correct stats
function. The function can refuse to work with a ``LogEntry`` object which doesn't
satisfy its requirements i.e. when the function needs information about the log
entry which the log file format doesn't have. 



.. vim:tw=80:ts=4:sw=4:fo+=n:ai:et 
